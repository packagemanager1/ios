// swift-tools-version: 5.7
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "ios",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "ios",
            targets: ["ios"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "ios",
            dependencies: ["RxSwift"], //"DelfinoMobileLibrary"
            path: "Sources"),
        .testTarget(
            name: "iosTests",
            dependencies: ["ios"],
            path: "Tests"),
        
//       birnary target's support type :'zip', 'xcframework', 'artifactbundle'
        .binaryTarget(
            name: "RxSwift",
            path: "Frameworks/RxSwift.xcframework"
        ),
        //Objective C로 된 framework 호출 방식 조사 필요(bridging-header와 같은 처리가 필요한가?)
        //Local binary
//        .binaryTarget(
//            name: "DelfinoMobileLibrary",
//            path: "Frameworks/DelfinoMobileLibrary.xcframework"
//        )
        
        
    ]
)
