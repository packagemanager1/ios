//
//  WCertFilter.h
//  DelfinoMobileLib
//
//  Created by yepark on 09/05/2018.
//  Copyright © 2018 wizvera. All rights reserved.
//

#import <Foundation/Foundation.h>


#ifndef WCertFilter_h
#define WCertFilter_h

#import "WCertificate.h"

@interface WCertFilter : NSObject

@property NSMutableDictionary* mCertFilterMap;

- (void)setIssuerCertFilter:(NSArray<NSString*>*)issuer;
- (void)setPolicyOidCertFilter:(NSArray<NSString*>*)oid;
- (void)setSubjectCertFilter:(NSString*)subject;
- (void)setSubjectArrayCertFilter:(NSArray<NSString*>*)subjects;
- (void)setSerialNumberCertFilter:(NSString*)serialNumber withFormat:(WNumberFormat)format;
- (void)setSerialNumberArrayCertFilter:(NSArray<NSString*>*)serialNumbers withFormat:(WNumberFormat)format;
- (BOOL)matchCertificate:(WCertificate*)cert;
@end

#endif /* WCertFilter_h */
