//
//  WPasswordGenerator.h
//  DelfinoMobileLibV2
//
//  Created by 이준형 on 2019. 4. 22..
//  Copyright © 2019년 wizvera. All rights reserved.
//

#ifndef WPasswordFactory_h
#define WPasswordFactory_h

#import <Foundation/Foundation.h>

#import "WPassword.h"
#import "WPasswordComplete.h"
#import "WCertID.h"

typedef NS_ENUM(int, WPasswordGeneratorMode) {
    WPasswordGeneratorMode_SET=0,
    WPasswordGeneratorMode_GET=1
};

@protocol WPassword;
@protocol WPKCS12Password;
@protocol WSecureKeyPad;
@protocol WPasswordAdapter;
@class WPasswordComplete;
@class WCertPinConfig;

@interface WPasswordGenerator : NSObject
- (id)initWithCertPin:(WCertPinConfig *)config;

- (id<WPassword>)create:(char *)password;
- (id<WPassword>)createWithData:(NSData*)pwdData;
- (id<WPKCS12Password>)createPKCS12Password:(char *)password;
- (id<WPKCS12Password>)createPKCS12PasswordWithData:(NSData*)pwdData;

- (id<WPassword>)createWithPasswordAdapter:(id<WPasswordAdapter>)passwordAdapter;

- (void)createWithPin:(WCertID *)cid mode:(WPasswordGeneratorMode)mode pin:(NSString *)pin complete:(WPasswordComplete *)complete;
- (void)createWithBiometrics:(WCertID *)cid mode:(WPasswordGeneratorMode)mode complete:(WPasswordComplete *)complete;
- (void)createWithPattern:(WCertID *)cid mode:(WPasswordGeneratorMode)mode pattern:(NSData *)pattern complete:(WPasswordComplete *)complete;

- (id<WPassword>)createBySecureKeyPad:(id<WSecureKeyPad>)secureKeyPad;
- (id<WPKCS12Password>)createPKCS12PasswordBySecureKeyPad:(id<WSecureKeyPad>)secureKeyPad;

@end


#endif /* WPasswordFactory_h */
