//
//  WCertKeyPair.h
//  DelfinoMobileLib
//
//  Created by 이준형 on 2019. 6. 5..
//  Copyright © 2019년 wizvera. All rights reserved.
//

#ifndef WCertKeyPair_h
#define WCertKeyPair_h

#import <Foundation/Foundation.h>

@interface WCertKeyPair : NSObject
/// 서명용 인증서 데이터(DER)
@property (nonatomic) NSData *signCert;
/// 서명용 인증서 개인키(DER)
@property (nonatomic) NSData *signPri;
/// 암호화용 인증서 데이터(DER)
@property (nonatomic) NSData *kmCert;
/// 암호화용 인증서 개인키(DER)
@property (nonatomic) NSData *kmPri;
@end


#endif /* WCertKeyPair_h */
