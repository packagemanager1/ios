
#import <Foundation/Foundation.h>

#import "WPassword.h"
#import "WPKCS12Password.h"
#import "WCertificate.h"

@protocol WPassword;
@protocol WPKCS12Password;
@class WCertificate;

@interface WCertID : NSObject

+ (WCertID*)createFromPKCS12:(NSData *)pkcs12 p12Password:(id<WPKCS12Password>)p12Password error:(int*)error;
- (BOOL)verifyVID:(NSString *)idn password:(id<WPassword>)password;
- (WCertificate *)getCertificate;
- (NSArray<NSNumber *>*)getPasswordTypes;

@end
