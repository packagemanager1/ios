//
//  WPasswordComplete.h
//  DelfinoMobileLib
//
//  Created by 이준형 on 2019. 4. 29..
//  Copyright © 2019년 wizvera. All rights reserved.
//

#ifndef WPasswordComplete_h
#define WPasswordComplete_h

#import <Foundation/Foundation.h>

#import "WPassword.h"

@interface WPasswordComplete : NSObject
/*!
 패스워드 생성 결과를 처리하는 델리게이트를 설정하는 함수

 @param target 델리게이트 타겟
 @param selector 패스워드 생성 후 호출할 함수의 selector
*/
- (void)setDelegate:(id)target selector:(SEL)selector;
@end

#endif /* WPasswordComplete_h */
