#ifndef WPassword_h
#define WPassword_h

#import <Foundation/Foundation.h>

typedef NS_ENUM(int, WPasswordType) {
    WPasswordType_Password=0,
    WPasswordType_NFC=1,
    WPasswordType_Fingerprint=2,
    WPasswordType_Faceprint=3,
    WPasswordType_Voiceprint=4,
    WPasswordType_Eyeprint=5,
    WPasswordType_Irisprint=6,
    WPasswordType_Handprint=7,
    WPasswordType_Actofsigning=8,
    WPasswordType_PIN=9,
    WPasswordType_Pattern=99
};

/**
 WPassword 프로토콜
 */
@protocol WPassword<NSObject>
@required
/// 패스워드의 타입을 리턴(WPasswordType)
- (WPasswordType)getType;
/// 패스워드를 리턴
- (NSData *)getSecret;
/// 패스워드 메모리 클리어
- (void)clear;
@end

#endif /* WPassword_h */
