
#import <Foundation/Foundation.h>

/**
 Secure Keypad 연동 프로토콜
 */
@protocol WSecureKeyPad<NSObject>
/// 비밀번호 가져오기
- (NSData *)getPassword;
/**
 비밀번호가 일치하는지 여부를 판별함
 @param secureKeyPad 일치여부를 판별할 비밀번호
 @return 일치하면 true
 */
- (BOOL)matchPassword:(id<WSecureKeyPad>)secureKeyPad;
/// 비밀번호 메모리 클리어
- (void)clear;

@end
