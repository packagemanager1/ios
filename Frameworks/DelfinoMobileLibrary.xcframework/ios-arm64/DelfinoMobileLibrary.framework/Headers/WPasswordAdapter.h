//
//  WPasswordAdapter.h
//  DelfinoMobileLib
//
//  Created by 이준형 on 2019. 4. 24..
//  Copyright © 2019년 wizvera. All rights reserved.
//

#ifndef WPasswordAdapter_h
#define WPasswordAdapter_h

#import <Foundation/Foundation.h>

#import "WPassword.h"

/**
 패스워드 어댑터 프로토콜
 */
@protocol WPasswordAdapter<NSObject>
@required
/// 패스워드의 타입 리턴
- (WPasswordType)getType;
/// 패스워드 리턴
- (NSData *)getSecret;
/// 패스워드의 메모리 클리어
- (void)clear;
@end

#endif /* WPasswordAdapter_h */
