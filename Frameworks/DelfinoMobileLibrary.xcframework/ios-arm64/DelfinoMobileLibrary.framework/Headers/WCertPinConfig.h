//
//  WCertPinConfig.h
//  DelfinoMobileLib
//
//  Created by 이준형 on 2019. 4. 24..
//  Copyright © 2019년 wizvera. All rights reserved.
//

#ifndef WCertPinConfig_h
#define WCertPinConfig_h

#import <Foundation/Foundation.h>

@interface WCertPinConfig : NSObject
/// CertPIN 서버 URL
@property (nonatomic) NSURL *serverUrl;
/// ssl 인증서 오류 무시(지원하지 않음)
@property (nonatomic) BOOL ignoreSslCertError;
/// 연결요청 timeout  - 사용하지 않음(readTimeoutMillis와 통합)
@property (nonatomic) int connectionTimeoutMillis;
/// 요청 timeout 설정 milliseconds
@property (nonatomic) int readTimeoutMillis;
@end

#endif /* WCertPinConfig_h */
