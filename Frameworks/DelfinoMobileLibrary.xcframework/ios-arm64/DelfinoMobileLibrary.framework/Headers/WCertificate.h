//
//  WCertificate.h
//  DelfinoMobileLib
//
//  Created by brson on 2017. 4. 13..
//  Copyright © 2017년 wizvera. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(int, WNumberFormat) {
    HEX,
    DECIMAL
};

typedef NS_ENUM(int, WCertStatus) {
    /** 유효 상태 */
    VALID=0,
    /** 폐기 상태 */
    REVOKED=1,
    /** 만료 상태 */
    EXPIRED=2,
    /** 무효 상태 */
    INVALID=3,
    /** 만료 예정 상태 */
    EXPIRE_IMMINENT=4
};


@interface WCertificate : NSObject

- (NSString*)getSubject;
- (NSString*)getSubjectValue:(NSString*)type;
- (NSArray<NSString*>*)getSubjectValues:(NSString*)type;
- (NSString*)getIssuer;
- (NSString*)getIssuerValue:(NSString*)type;
- (NSArray<NSString*>*)getIssuerValues:(NSString*)type;
- (NSString*)getIssuerName;
- (NSString*)getPolicyOID;
- (NSArray<NSString*>*)getPolicyOIDs;
- (NSString*)getPolicyName;
- (NSString*)getPolicyName:(NSString*)language;
- (NSString*)getSerialNumber:(WNumberFormat)numberFormat;
- (NSDate*) getNotAfter;
- (NSDate*)getNotBefore;

- (WCertStatus)validate;
- (WCertStatus)validate:(BOOL)checkCRL;
- (int)getVersion;
- (NSString*)getSigAlgName;
- (BOOL)isBrowserCert;

- (NSString*)getSha256Fingerprint;
- (NSString*)getSha1Fingerprint;
- (NSString*)getFingerprint;
@end
