#import <Foundation/Foundation.h>

#import "WPassword.h"

@class WCertID;
@class WCertFilter;
@class WCertKeyPair;
@protocol WPassword;
@protocol WPKCS12Password;

@interface WCertStore : NSObject

+ (WCertStore*)getInstance;
+ (WCertStore*)getKeychainStore:(NSString *)keychainAccessGroup;
+ (WCertStore*)getFileStore;

- (NSArray*)getCertList:(WCertFilter*)certFilter;
- (NSArray*)getCertList;
- (NSData*)exportPKCS12:(WCertID*)cid password:(id<WPassword>)password p12Password:(id<WPKCS12Password>)p12Password error:(int*)error;
- (WCertID*)importPKCS12:(NSData*)pkcs12 p12Password:(id<WPKCS12Password>)p12Password password:(id<WPassword>)password error:(int*)error;
- (NSData*)exportCertificate:(WCertID*)cid;
- (int)removeCert:(WCertID*)cid;
- (int)changePassword:(WCertID*)cid password:(id<WPassword>)password newPassword:(id<WPassword> )newPassword;

- (int)addPassword:(WCertID*)cid password:(id<WPassword>)password newPassword:(id<WPassword>)newPassword;
- (int)removePassword:(WCertID*)cid passwordType:(WPasswordType)passwordType;
- (int)checkPassword:(WCertID*)cid password:(id<WPassword>)password;

- (WCertID *)importCertKeyPair:(WCertKeyPair *)certKeyPair error:(int *)error;
- (WCertKeyPair *)exportCertKeyPair:(WCertID *)cid error:(int *)error;



@end
