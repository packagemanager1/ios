//
//  WPKCS12.h
//  DelfinoMobileLib
//
//  Copyright © 2019년 wizvera. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WPKCS12Password.h"

@protocol WPKCS12Password;
@class WCertKeyPair;

@interface WPKCS12 : NSObject
+ (int)checkPassword:(NSData *)pkcs12 password:(id<WPKCS12Password>)password;
+ (NSData *)changePassword:(NSData *)pkcs12 password:(id<WPKCS12Password>)password newPassword:(id<WPKCS12Password>)newPassword error:(int *)error;
+ (NSData *)create:(WCertKeyPair *)certKeyPair password:(id<WPKCS12Password>)password error:(int *)error;
+ (WCertKeyPair *)parse:(NSData *)pkcs12 password:(id<WPKCS12Password>)password error:(int *)error;
@end
