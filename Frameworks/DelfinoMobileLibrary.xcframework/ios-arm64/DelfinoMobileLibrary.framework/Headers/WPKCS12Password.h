//
//  WPKCS12Password.h
//  DelfinoMobileLib
//
//  Created by 이준형 on 2019. 4. 23..
//  Copyright © 2019년 wizvera. All rights reserved.
//

#ifndef WPKCS12Password_h
#define WPKCS12Password_h

#import <Foundation/Foundation.h>

#import "WPassword.h"

@protocol WPassword;

/**
 PKCS12의 패스워드 프로토콜. 텍스트 타입 비밀번호만 지원
 */
@protocol WPKCS12Password<NSObject>
/// WPassword타입 객체로 리턴
- (id<WPassword>)getPassword;
/// 패스워드 메모리 클리어
- (void)clear;
@end


#endif /* WPKCS12Password_h */
