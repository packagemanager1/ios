//
//  WUtil.h
//  DelfinoMobileLib
//
//  Created by brson on 2017. 4. 14..
//  Copyright © 2017년 wizvera. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WUtil : NSObject

+ (NSString*)encodeHex:(NSData*)data;
+ (NSData*)decodeHex:(NSString*)str;

+ (NSString*)encodeBase64:(NSData*)data;
+ (NSData*)decodeBase64:(NSString*)str;

+ (NSString*)string2JsonString:(NSString*)jsonString;
@end
