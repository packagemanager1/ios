//
//  WCMP.h
//  DelfinoMobileLib
//
//  Copyright © 2019년 wizvera. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "WCmpComplete.h"

@class WCertID;
@protocol WPassword;
@class WCertStore;
@class WCmpComplete;

typedef NS_ENUM(int, WCAType) {
    CA_yessign,
    CA_signkorea,
    CA_kica, 
    CA_crosscert,
    CA_tradesign,
    CA_iniPass
};

@interface WCMPResult : NSObject
/// CMP 처리 에러코드. 성공시 0
@property (readonly) int errorCode;
/// CMP 에러메시지
@property (readonly) NSString* errorMessage;
@end

@interface WCmp : NSObject

- (id)initWithServer:(WCAType)caType host:(NSString *)host port:(int)port;
- (id)initWithUrl:(WCAType)caType url:(NSString *)url;

- (void)issueCert:(NSString *)refCode authNum:(NSString *)authNum password:(id<WPassword>)password certstore:(WCertStore *)certstore complete:(WCmpComplete *)complete;
- (void)reissueCert:(NSString *)refCode authNum:(NSString *)authNum password:(id<WPassword>)password certstore:(WCertStore *)certstore complete:(WCmpComplete *)complete;
- (void)renewCert:(WCertID *)cid password:(id<WPassword>)password newPassword:(id<WPassword>)newPassword certstore:(WCertStore *)certstore complete:(WCmpComplete *)complete;

@end
