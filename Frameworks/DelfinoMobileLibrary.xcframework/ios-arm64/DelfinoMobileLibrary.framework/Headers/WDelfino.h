
#import <Foundation/Foundation.h>

#import "WCertStore.h"
#import "WCertID.h"
#import "WCertificate.h"
#import "WSign.h"
#import "WUtil.h"
#import "WError.h"
#import "WCertFilter.h"
#import "WCmp.h"
#import "WPassword.h"
#import "WPasswordGenerator.h"
#import "WPasswordAdapter.h"
#import "WPKCS12.h"
#import "WPKCS12Password.h"
#import "WSecureKeyPad.h"
#import "WCertPinConfig.h"
#import "WPasswordComplete.h"
#import "WCmpComplete.h"
#import "WCertKeyPair.h"

#define DelfinoMobileLib_Version @"2.8.4"

@interface WDelfino : NSObject
+ (NSString*)version;
@end
