//
//  WError.h
//  DelfinoMobileLib
//
//  Created by brson on 2017. 4. 17..
//  Copyright © 2017년 wizvera. All rights reserved.
//

#ifndef WError_h
#define WError_h

#define DelfinoMobileLibErrorDomain     @"com.wizvera.delfinomobilelib.ErrorDomain"

#define WERR_OK                            0
#define WERR_INVALID_PARAMETER           -11

#define WERR_INVALID_CERT_DATA           -101
#define WERR_INVALID_PRIKEY_DATA         -102
#define WERR_PASSWORD_INCORRECT          -103
#define WERR_NOT_MATCH_VID               -104

#define WERR_INVALID_PKCS12_DATA         -121
#define WERR_PKCS12_NOT_CONTAIN_SIGNCERT -122
#define WERR_PKCS12_NOT_CONTAIN_SIGNPRI  -123
#define WERR_PKCS12_CREATE_FAIL          -124

#define WERR_INVALID_SIGN_TYPE           -131

#define WERR_UCPID_REQUEST_INFO_ENCODING_FAIL -141
#define WERR_UCPID_NOT_VALID_BASE64URL_ENCODING -142

#define WERR_FILE_WRITE_FAIL             -201
#define WERR_FILE_REMOVE_FAIL            -202
#define WERR_INVALID_ENCRYPT_FILE        -203
#define WERR_AES_KEY_LOAD_FAIL           -204
#define WERR_AES_KEY_SAVE_FAIL           -205
#define WERR_FILE_DECRYPT_FAIL           -206
#define WERR_FILE_ENCRYPT_FAIL           -207

#define WERR_NEWPASSWORD_AND_OLDPASSWORD_IS_SAME -401
#define WERR_NEWPASSWORD_AND_NEWPASSWORD_CONFIRM_IS_DIFFERENT -402
#define WERR_NEWPASSWORD_IS_NOT_VALID_PW_POLICY -403

#define WERR_CMS_ERROR                        -500
#define WERR_CMS_INVALID_SIGNED_DATA          -501
#define WERR_CMS_SIGNED_DATA_VERIFY_FAILED    -502
#define WERR_CMS_MISMATCH_MESSAGE_DIGEST      -503
#define WERR_CMS_INVALID_SHA256_MD            -511

#define WERR_CMP_ERROR                  -600
#define WERR_CMP_NOT_SUPPORTED_CA       -601
#define WERR_CMP_NETWORK_ERROR          -602
#define WERR_CMP_SERVER_ERROR           -603
#define WERR_CMP_BAD_PASSWORD_TYPE      -604

#define WERR_CERTPIN_ERROR_RESPONSE                -800
#define WERR_CERTPIN_INVALID_PIN_LENGTH            -801
#define WERR_CERTPIN_INVALID_PIN_3_SAME_NUMBER     -802
#define WERR_CERTPIN_INVALID_PIN_3_SEQ_NUBMER      -803
#define WERR_CERTPIN_INVALID_PATTERN_LENGTH        -804
#define WERR_CERTPIN_NOT_INITIALIZED               -811
#define WERR_CERTPIN_INVALID_MODE                  -812
#define WERR_CERTPIN_ERR_BIO_NOT_SUPPORT           -814
#define WERR_CERTPIN_ERR_BIO_EVALUATE_FAILED       -815

//#define WERR_CERTPIN_ERROR                   -700

#endif /* WError_h */
