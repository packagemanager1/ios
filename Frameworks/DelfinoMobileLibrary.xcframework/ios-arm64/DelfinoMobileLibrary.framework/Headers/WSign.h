//
//  WCMS.h
//  DelfinoMobileLib
//
//  Created by brson on 2017. 4. 13..
//  Copyright © 2017년 wizvera. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WCertID.h"
#import "WPassword.h"

@class WCertID;
@protocol WPassword;

typedef NS_ENUM(int, WSignType) {
    CMS_SIGNED_DATA,
    SIGNATURE
};

/**
 ucpidSign에 사용되는 사용자 정보
 */
@interface WUserAgreeInfo : NSObject
/// realName
@property BOOL realName;
/// gender
@property BOOL gender;
/// nationalInfo
@property BOOL nationalInfo;
/// birthDate;
@property BOOL birthDate;
/// ci
@property BOOL ci;
@end

/**
 서명결과 정보
 */
@interface WSignResult : NSObject
/// 전자서명 값
@property (readonly, nonnull) NSString *signedData;
/// Vid 값
@property (readonly, nonnull) NSString *vidRandom;
@end


NS_ASSUME_NONNULL_BEGIN

@interface WSign : NSObject
+ (WSignResult *_Nullable)sign:(WCertID * _Nullable)cid password:(id<WPassword>_Nullable)password signType:(WSignType)signType data:(NSData *)data error:(int *)error;

+ (WSignResult *_Nullable)ucpidSign:(WCertID *_Nullable)cid password:(id<WPassword>_Nullable)password
                         ucpidNonce:(NSString*)ucpidNonce
                      userAgreement:(NSString *)userAgreement
                      userAgreeInfo:(WUserAgreeInfo *)userAgreeInfo
                         ispInfoUrl:(NSString*)ispInfoUrl
                              error:(int *)error;

+ (WSignResult *_Nullable)mdSign:(WCertID *_Nullable)cid password:(id<WPassword>_Nullable)password sha256MD:(NSData *)sha256MD error:(int *)error;
+ (WSignResult *_Nullable)mdSignAdd:(WCertID *_Nullable)cid password:(id<WPassword>_Nullable)password detachedSignedDataDER:(NSData *)detachedSignedDataDER error:(int *)error;
+ (NSData *_Nullable)fileSign:(WCertID *_Nullable)cid password:(id<WPassword>_Nullable)password fileName:(NSString *)fileName fileContent:(NSData *)fileContent error:(int *)error;
+ (NSData *_Nullable)fileSignAdd:(WCertID *_Nullable)cid password:(id<WPassword>_Nullable)password fileSignedData:(NSData *)fileSignedData error:(int *)error;

+ (WSignResult *_Nullable)sign:(WCertID *_Nullable)cid password:(id<WPassword>_Nullable)password signType:(WSignType)signType data:(NSData *)data signingTime:(time_t)signingTime error:(int *)error;
+ (WSignResult *_Nullable)ucpidSign:(WCertID *_Nullable)cid password:(id<WPassword>_Nullable)password
                         ucpidNonce:(NSString*)ucpidNonce
                      userAgreement:(NSString *)userAgreement
                      userAgreeInfo:(WUserAgreeInfo *)userAgreeInfo
                         ispInfoUrl:(NSString*)ispInfoUrl
                        signingTime:(time_t)signingTime
                              error:(int *)error;

+ (WSignResult *_Nullable)mdSign:(WCertID *_Nullable)cid password:(id<WPassword>_Nullable)password sha256MD:(NSData *)sha256MD signingTime:(time_t)signingTime error:(int *)error;
+ (WSignResult *_Nullable)mdSignAdd:(WCertID *_Nullable)cid password:(id<WPassword>_Nullable)password detachedSignedDataDER:(NSData *)detachedSignedDataDER signingTime:(time_t)signingTime error:(int *)error;
+ (NSData *_Nullable)fileSign:(WCertID *_Nullable)cid password:(id<WPassword>_Nullable)password fileName:(NSString *)fileName fileContent:(NSData *)fileContent signingTime:(time_t)signingTime error:(int *)error;
+ (NSData *_Nullable)fileSignAdd:(WCertID *_Nullable)cid password:(id<WPassword>_Nullable)password fileSignedData:(NSData *)fileSignedData signingTime:(time_t)signingTime error:(int *)error;

+ (NSString *_Nullable)multiSignForMyData:(WCertID *_Nullable)cid password:(id<WPassword>_Nullable)password
                    signInfoJson:(NSString *)signInfoJson
                              error:(int *)error;

+ (NSString *_Nullable)multiSignForMyData:(WCertID *_Nullable)cid password:(id<WPassword>_Nullable)password
                    signInfoJson:(NSString *)signInfoJson
                              signingTime:(time_t)signingTime error:(int *)error;

@end


NS_ASSUME_NONNULL_END
