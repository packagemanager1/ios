//
//  WCmpComplete.h
//  DelfinoMobileLib
//
//  Created by 이준형 on 2019. 4. 29..
//  Copyright © 2019년 wizvera. All rights reserved.
//

#ifndef WCmpComplete_h
#define WCmpComplete_h

#import <Foundation/Foundation.h>

@interface WCmpComplete : NSObject
/*!
 CMP결과를 처리하는 델리게이트를 설정하는 함수
 
 @param target 델리게이트 타겟
 @param selector CMP 호출 완료 후 호출할 함수의 selector
 
 @discussion selector 함수는 -(void)getCMPResult:(WCertID *)cid result:(WCMPResult *)result 의 형식
 
 `````
 // cid가 nil이면 발급실패
 - (void)getCMPResult:(WCertID *)cid result:(WCMPResult *)result
 {
 ...
 WCertificate *cert = [cid getCertificate];
 ...
 }
 
 ...
 WCmpComplete complete = [WCmpComplete new];
 [complete setDelegate:self selector:@selector(getCMPResult:result:)];
 
 `````
 */

- (void)setDelegate:(id)target selector:(SEL)selector;
@end


#endif /* WCmpComplete_h */
